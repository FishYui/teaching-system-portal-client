auth('', './portal/login.html');

var app = new Vue({
    el: '#Body',
    data: {
        courseList: [],
    },
    mounted: async function () {
        const response = await Fetch(`${portalUrl}/course/resources`, {
            credentials: 'include'
        });
        if (response.success) {

            response.data.entry.map(app => {
                this.courseList.push({
                    "id": app.resource.slot[0].reference.split('/')[1],
                    "name": app.resource.slot[0].display,
                });
            })
        }
    },
    methods: {
        clickList(even) {
            const slotId = even.target.className;
            location.href = `./portal/course.html?slotId=${slotId}`;
        }
    }
})