auth('', '../portal/login.html');

var app = new Vue({
    el: '#Body',
    data: {
        courseName: '',
        documentReferences: [],
        token: ''
    },
    mounted: async function () {

        const slotId = await QueryString('slotId');

        const response = await Fetch(`${portalUrl}/course/documentReferences?slotId=${slotId}`, {
            credentials: 'include'
        });

        if (response.success === true && response.data) {
            
            this.courseName = response.data.slot.comment;
            
            response.data.documentReference.entry.map(dr => {
                this.documentReferences.push({
                    'name': dr.resource.description,
                    'content': dr.resource.content ? dr.resource.content.map(content => {
                        return {
                            'title': content.attachment.title,
                            'authUrl': `${portalUrl}/course/documentReference/${dr.resource.id}?slotId=${slotId}&attachmentUrl=${content.attachment.url}`
                        }
                    }) : ''
                });
            });
        }
    },
    methods: {
        async clickContent(e, authUrl, attachmentTitle) {

            e.preventDefault();
            
            try {
                const response = await Fetch((`${authUrl}`), {
                    credentials: 'include'
                });

                if (response.success) {
                    const token = response.token
                    location.href = `${materialUrl}/resource/download?token=${token}`;
                } else {
                    alert(response.message);
                }
            } catch (err) {
                alert(err);
                console.log(err);
            }
        }
    }
})