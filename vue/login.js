auth('../index.html', '');

var app = new Vue({
    el: '#body',
    data: {
        message: '',
        username: '',
        password: ''
    },
    methods: {
        login: async function () {
            
            this.message = "";

            if (this.username && this.password) {

                try {

                    const response = await Fetch(`${portalUrl}/login`, {
                        method: 'POST',
                        credentials: 'include',
                        body: JSON.stringify({ username: this.username, password: this.password }),
                        headers: {
                            'content-Type': 'application/json'
                        },
                    });

                    if (response.success === true) {
                        location.href = '../index.html';
                    } else {
                        this.message = response.message;
                    }

                } catch (err) {
                    alert(`Server Error: ${err.message}`);
                }

            } else {
                this.message = '請輸入帳號和密碼!';
            }
        }
    }
});