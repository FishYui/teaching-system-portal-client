function auth(successUrl, failUrl) {

    try {

        const response = Fetch(`${portalUrl}/auth`, {
            method: 'GET',
            credentials: 'include',
            headers: {
                'content-type': 'application/json'
            }
        });

        if (response.success === true && successUrl) {
            location.href = successUrl;
        } else if (response.success === false && failUrl) {
            location.href = failUrl;
        }

    } catch (err) {
        alert(`Server Error: ${err.message}`);
        if (location.href != `${seltUrl}/login.html`) {
            location.href = '/portal/login.html';
        }
    }

}

async function Fetch(url, options) {

    const opt = options || {};

    const response = await fetch(url, opt);

    if (response.status >= 200 && response.status < 300) {
        const responseType = response.headers.get('content-type');
        if (responseType.match('application/json')) {
            return await response.json();
        } else {
            return await response.blob();
        }

    } else {
        const error = new Error(await response.statusText);
        error.response = response;
        throw error;
    }
}

function QueryString(name) {
    var str = location.search.substring(1).split("&");
    for (i = 0; i < str.length; i++) {
        var result = str[i].split("=");
        if (result[0] == name) return result[1];
    }
    return "";
}

document.getElementsByClassName('logout')[0].onclick = async function () {
    await fetch(`${portalUrl}/logout`, { credentials: 'include', });
    location.href = '/portal/login.html';
};